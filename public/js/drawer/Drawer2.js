var Drawer = (function () {
    function Drawer(canvas, h, w) {
        this.canvas = document.getElementById(canvas);
        this.context = this.canvas.getContext('2d');
        this.canvas.height = h;
        this.canvas.width = w;
    }
    /* DRAW FUNCTIONS */
    Drawer.prototype.clearCanvas = function () {
        this.context.clearRect(0, 0, this.canvas.width, this.canvas.height);
    };
    Drawer.prototype.drawPoint = function (point, color) {
        if (color !== 'undefined') {
            this.context.fillStyle = color;
        }
        this.context.beginPath();
        this.context.arc(point.x, point.y, 2, 0, 2 * Math.PI, false);
        this.context.fill();
    };
    Drawer.prototype.drawPoints = function (points, color) {
        for (var i = 0; i < points.length; i++) {
            this.drawPoint(points[i], color);
        }
    };
    Drawer.prototype.drawPoligon = function (points, color) {
        if (color !== 'undefined') {
            this.context.strokeStyle = color;
        }
        this.context.beginPath();
        this.context.moveTo(points[0].x, points[0].y);
        for (var i = 1; i < points.length; i++) {
            this.context.lineTo(points[i].x, points[i].y);
        }
        this.context.stroke();
    };
    return Drawer;
}());
