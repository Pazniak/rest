class Drawer {
    canvas: any;
    context: any;

    constructor(canvas: any, h: number, w: number) {
        this.canvas = document.getElementById(canvas);
        this.context = this.canvas.getContext('2d');
        this.canvas.height = h;
        this.canvas.width = w;
    }

    /* DRAW FUNCTIONS */

    clearCanvas(): void {
        this.context.clearRect(0, 0, this.canvas.width, this.canvas.height);
    }

    drawPoint(point: any, color: string): void {
        if(color !== 'undefined') {
            this.context.fillStyle = color;
        }
        this.context.beginPath();
        this.context.arc(point.x, point.y, 2, 0, 2 * Math.PI, false);
        this.context.fill();
    }

    drawPoints(points: any, color: string) {
        for (let i = 0; i < points.length; i++) {
            this.drawPoint(points[i], color);
        }
    }

    drawPoligon(points: any[], color: string) {
        if (color !== 'undefined') {
            this.context.strokeStyle = color;
        }
        this.context.beginPath();
        this.context.moveTo(points[0].x, points[0].y);
        for(let i = 1; i < points.length; i++) {
            this.context.lineTo(points[i].x, points[i].y);
        }
        this.context.stroke();
    }
}
