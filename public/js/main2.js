'use strict';
/// <reference path="index.d.ts" />
var Point = (function () {
    function Point(x, y) {
        this.x = x;
        this.y = y;
    }
    return Point;
}());
var Drawer = (function () {
    function Drawer(canvas, h, w) {
        this.canvas = document.getElementById(canvas);
        this.context = this.canvas.getContext('2d');
        this.canvas.height = h;
        this.canvas.width = w;
    }
    /* DRAW FUNCTIONS */
    Drawer.prototype.clearCanvas = function () {
        this.context.clearRect(0, 0, this.canvas.width, this.canvas.height);
    };
    Drawer.prototype.drawPoint = function (point, color) {
        if (color !== 'undefined') {
            this.context.fillStyle = color;
        }
        this.context.beginPath();
        this.context.arc(point.x, point.y, 2, 0, 2 * Math.PI, false);
        this.context.fill();
    };
    Drawer.prototype.drawPoints = function (points, color) {
        for (var i = 0; i < points.length; i++) {
            this.drawPoint(points[i], color);
        }
    };
    Drawer.prototype.drawPoligon = function (points, color) {
        if (color !== 'undefined') {
            this.context.strokeStyle = color;
        }
        this.context.beginPath();
        this.context.moveTo(points[0].x, points[0].y);
        for (var i = 1; i < points.length; i++) {
            this.context.lineTo(points[i].x, points[i].y);
        }
        this.context.stroke();
    };
    return Drawer;
}());
var CANVAS_HEIGHT = 300;
var CANVAS_WIDTH = 300;
var drawer = new Drawer('canvas', CANVAS_HEIGHT, CANVAS_WIDTH);
var points_data;
var chain_data;
var pictures_data;
var drawPointsElement = 'drawPoints', drawHullElement = 'drawHull', drawTriangulationElement = 'drawTriangulation', clearCanvasElement = 'clearCanvas', savePictureElement = 'savePicture', getPicturesElement = 'getPictures', squareElement = 'square', picturesContainerElement = 'picturesContainer', signInElement = 'signIn', signUpElement = 'signUp';
var drawPoints = document.getElementById(drawPointsElement);
var drawHull = document.getElementById(drawHullElement);
var drawTriangulation = document.getElementById(drawTriangulationElement);
var clearCanvas = document.getElementById(clearCanvasElement);
var savePicture = document.getElementById(savePictureElement);
var pictures = document.getElementById(getPicturesElement);
var squareContainer = document.getElementById(squareElement);
var picturesContainer = document.getElementById(picturesContainerElement);
var signIn = document.getElementById(signInElement);
var signUp = document.getElementById(signUpElement);
function getHull(points) {
    swapPoints(points[0], points[getMaxY(points)]);
    swapPoints(points[1], points[getMinY(points)]);
    var chain;
    if (isRighter(points[2], points[0], points[1]) === 1) {
        chain = [points[0], points[2], points[1]];
    }
    else {
        chain = [points[0], points[1], points[2]];
    }
    chain.push(chain[0]);
    for (var i = 3; i < points.length; i++) {
        var res = getVisibleSides(points[i], chain);
        if (res.length !== 0) {
            var chain_arr1 = chain.slice(0, res[0] + 1);
            var chain_arr2 = chain.slice(res[1]);
            chain = chain_arr1.concat(points[i], chain_arr2);
        }
    }
    return chain;
}
drawHull.onclick = function () {
    drawer.clearCanvas();
    var number = document.getElementById('number').value;
    var point_number = parseInt(number);
    var on_success = function (points) {
        points_data = points;
        drawer.drawPoints(points, '#26244a');
        chain_data = getHull(points);
        drawer.drawPoligon(chain_data, '#1A9CB0');
        squareContainer.innerHTML = 'Hull square: ' + getPoligonSquare(chain_data).toFixed(2);
    };
    genRandomPoints(point_number, on_success);
};
clearCanvas.onclick = function () {
    drawer.clearCanvas();
};
savePicture.onclick = function () {
    var on_success = function (info) {
        console.log(info);
    };
    savePictureRequest(on_success);
};
pictures.onclick = function () {
    getPictures(drawPictures);
};
signIn.onclick = function () {
    var on_success = function (info) {
        console.log(info);
    };
    signInRequest(getUserData(), on_success);
};
function getUserData() {
    var user_data = {
        login: document.getElementById('login').value,
        password: document.getElementById('password').value
    };
    return user_data;
}
/* REQUESTS */
function genRandomPoints(number, on_succ) {
    var url = '/points?number=' + number;
    var response = $.get(url, function (data) {
        on_succ(data);
    });
}
function getPictures(on_succ) {
    var url = '/pictures';
    var response = $.get(url, function (data) {
        pictures_data = data;
        on_succ(data);
    });
}
function savePictureRequest(on_succ) {
    var picture_info = {
        points: points_data,
        chain: chain_data
    };
    var url = '/pictures?pictureinfo=' + JSON.stringify(picture_info);
    $.post(url, function (data) {
        on_succ(data);
    });
}
function removePictureRequest(index, on_succ) {
    var url = '/pictures?index=' + index;
    $.ajax({
        url: url,
        type: 'DELETE',
        success: on_succ
    });
}
function signInRequest(user_data, on_succ) {
    var url = '/signIn?login=' + user_data.login + '&password=' + user_data.password;
    $.post(url, function (data) {
        on_succ(data);
    });
}
function removePicture(index) {
    var on_succes = function (data) {
        console.log(data);
    };
    removePictureRequest(index, on_succes);
    var pictures = pictures_data.slice(0, index).concat(pictures_data.slice(index + 1));
    drawPictures(pictures);
}
function drawPictures(pictures_data) {
    picturesContainer.innerHTML = null;
    for (var i = 0; i < pictures_data.length; i++) {
        var newCanvas = document.createElement('canvas');
        var canvasContainer = document.createElement('div');
        var closeLink = document.createElement('a');
        closeLink.setAttribute('class', 'removePicture');
        closeLink.setAttribute('href', '#');
        closeLink.setAttribute('onclick', 'removePicture(' + i + ')');
        newCanvas.setAttribute('id', 'canvas' + i);
        newCanvas.setAttribute('class', 'white');
        canvasContainer.setAttribute('class', 'canvas-container');
        canvasContainer.appendChild(newCanvas);
        canvasContainer.appendChild(closeLink);
        picturesContainer.appendChild(canvasContainer);
        var drawer = new Drawer('canvas' + i, CANVAS_HEIGHT, CANVAS_WIDTH);
        drawPicture(drawer, pictures_data[i]);
    }
}
function drawPicture(drawer, picture_data) {
    var points = picture_data.points;
    var chain = picture_data.chain;
    drawer.drawPoints(points, '#26244a');
    drawer.drawPoligon(chain, '#39c2d7');
}
/* LOGIC FUNCTIONS */
function getPoligonSquare(chain) {
    var min_y = points_data[getMaxY(points_data)], max_y = points_data[getMinY(points_data)];
    var central_point = new Point((min_y.x + max_y.x) / 2, (min_y.y + max_y.y) / 2);
    drawer.drawPoint(central_point, 'magenta');
    var common_square = 0;
    for (var i = 0; i < chain.length - 1; i++) {
        common_square += getTriangleSquare(chain[i], chain[i + 1], central_point);
    }
    return common_square;
}
function getTriangleSquare(p1, p2, p3) {
    var a = getDistance(p1, p2), b = getDistance(p2, p3), c = getDistance(p1, p3);
    var p = (a + b + c) / 2;
    var square = Math.sqrt(p * (p - a) * (p - b) * (p - c));
    return square;
}
function getVisibleSides(point, chain) {
    var result = [];
    if (isRighter(point, chain[0], chain[1]) === 1) {
        result.push(0);
    }
    for (var j = 0; j < chain.length - 2; j++) {
        var a = isRighter(point, chain[j], chain[j + 1]), b = isRighter(point, chain[j + 1], chain[j + 2]);
        if (a * b === -1 || (a === 0 && b === 1) || (b === 0 && a === 1)) {
            result.push(j + 1);
        }
    }
    if (result.length === 1) {
        result.push(chain.length - 1);
    }
    return result;
}
function swapPoints(a, b) {
    var temp = new Point(a.x, a.y);
    a.x = b.x;
    a.y = b.y;
    b.x = temp.x;
    b.y = temp.y;
}
// return index
function getMaxY(points) {
    var max_y = points[0].y, max_index = 0;
    for (var i = 1; i < points.length; i++) {
        if (points[i].y > max_y) {
            max_y = points[i].y;
            max_index = i;
        }
    }
    return max_index;
}
// return index
function getMinY(points) {
    var min_y = points[0].y, min_index = 0;
    for (var i = 1; i < points.length; i++) {
        if (points[i].y < min_y) {
            min_y = points[i].y;
            min_index = i;
        }
    }
    return min_index;
}
// return index
function getMaxX(points) {
    var max_x = points[0].x, max_index = 0;
    for (var i = 1; i < points.length; i++) {
        if (points[i].y > max_x) {
            max_x = points[i].y;
            max_index = i;
        }
    }
    return max_index;
}
// return index
function getMinX(points) {
    var min_x = points[0].x, min_index = 0;
    for (var i = 1; i < points.length; i++) {
        if (points[i].y < min_x) {
            min_x = points[i].y;
            min_index = i;
        }
    }
    return min_index;
}
function isRighter(point, start_point, end_point) {
    var x = point.x, y = point.y, x1 = start_point.x, y1 = start_point.y, x2 = end_point.x, y2 = end_point.y;
    var result = (x2 - x) * (y1 - y) - (y2 - y) * (x1 - x);
    if (result > 0) {
        return -1;
    }
    else if (result === 0) {
        return 0;
    }
    else {
        return 1;
    }
}
function getDistance(point1, point2) {
    var distance = Math.sqrt((point1.x - point2.x) * (point1.x - point2.x) + (point1.y - point2.y) * (point1.y - point2.y));
    return distance;
}
