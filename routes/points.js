var express = require('express');
var router = express.Router();

var CANVAS_WIDTH = 300;
var CANVAS_HEIGHT = 300;

function genRandomPoints(number) {
    var result = [];
    for(var i = 0; i < number; i++) {
        result.push(genRandomPoint());
    }
    console.log('points: ', result);
    return result;
}

function genRandomPoint() {
    var x = getRandomInt(10, CANVAS_WIDTH - 10),
        y = getRandomInt(10, CANVAS_HEIGHT - 10);
    var point = { x: x, y: y };
    console.log('point: ', point);
    return point;
}

function getRandomInt(min, max) {
    var res = 0;
    for (var i = 0; i < 3; i++) {
        res += Math.random() * (max - min) + min;
    }
    return Math.floor(res / 3);
}

router.get('', function(req, res, next) {
    var number = parseInt(req.query.number);
    console.log(number);
    var points = genRandomPoints(number);
    return res.send(points);
});

module.exports = router;
