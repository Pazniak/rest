var express = require('express');
var router = express.Router();
var bodyParser = require('body-parser');
var fs = require('fs');
var db_path = './db/picturesDB.js';

function updateDB(new_picture) {
    var db = readDB();
    db.pictures.push(new_picture);
    fs.writeFileSync(db_path, JSON.stringify(db, null, '\t'));
}

function readDB() {
    var db = JSON.parse(fs.readFileSync(db_path));
    return db;
}

router.post('/', function (req, res) {
    var picture_info = JSON.parse(req.query.pictureinfo);
    var picture = {
        points: picture_info.points,
        chain: picture_info.chain
    };
    updateDB(picture);
    res.send('succes!');
});

router.get('/', function (req, res) {
    var pictures = readDB().pictures;
    if (pictures) {
        return res.send(pictures);
    } else {
        return res.send({ error: 'Server error' });
    }
});

router.delete('/', function (req, res) {
    var db = readDB();
    var index = parseInt(req.query.index);
    db.pictures = db.pictures.slice(0, index).concat(db.pictures.slice(index + 1));
    fs.writeFileSync(db_path, JSON.stringify(db, null, '\t'));
    return res.send('succes!');
})

module.exports = router;
