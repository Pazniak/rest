var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
    console.log('Main page requested!');
    res.render('index', { title: 'Express',
                          name:  'Alena Pazniak' });
});

module.exports = router;
