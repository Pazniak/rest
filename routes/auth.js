var passport = require('passport');
var express = require('express');
var router = express.Router();

var fs = require('fs');
var db_path = './db/usersDB.js';

function readDB() {
    var db = JSON.parse(fs.readFileSync(db_path));
    return db;
}

function appendDB(user) {
    var db = readDB();
    db.users.push(user);
    fs.writeFileSync(db_path, JSON.stringify(db, null, '\t'));
    return db;
}

function deleteDBbyName(name) {
    var db = readDB();
    var idx = db.users.findIndex(function (u) {
        return (u.username === name);
    });
    if (idx != -1) {
        db.users = db.users.slice(0, idx).concat(db.users.slice(idx + 1));
        fs.writeFileSync(db_path, JSON.stringify(db, null, '\t'));
    }
    return db;
}

router.get('/', function (req, res) {
    if (req.isAuthenticated()) {
        res.send('Your login is: ' + req.user.username);
        return;
    }

    res.send('You are not logged in');
});

router.get('/sign-up', function (req, res) {
    var uname = req.query.username;
    var upass = req.query.password;

    var db = appendDB({
        "username": uname,
        "password": upass
    });

    console.log(db);

    res.send(uname + ' account created. Please, try to login');
});

router.get('/sign-out', function (req, res) {
    var uname = req.user.username;
    req.logout();
    res.send(uname + ' is logouted');
});

router.get('/delete', function (req, res) {
    var uname = req.query.username;

    var db = deleteDBbyName(uname);

    console.log(db);

    res.send(uname + ' is deleted');
});

router.post('/', passport.authenticate('local'), function(req, res) {
    // If this function gets called, authentication was successful.
    // `req.user` contains the authenticated user.
    res.send(req.user.username + ' successfully logged in');
});

module.exports = router;