var passport = require('passport');
var AuthLocalStrategy = require('passport-local').Strategy;

var fs = require('fs');
var db_path = './db/usersDB.js';

function readDB() {
    var db = JSON.parse(fs.readFileSync(db_path));
    return db;
}

passport.use('local', new AuthLocalStrategy(
    function (username, password, done) {
        console.log('Log in: ' + username + ', Pass: ' + password);
 
        var db = readDB();
        var user = db.users.find(function (u) {
            return (u.username === username) && (u.password === password);
        });

        if (user) {
            return done(null, {
                username: user.username
            });
        }
 
        return done(null, false, { 
            message: 'Неверный логин или пароль' 
        });
    }
));

passport.serializeUser(function (user, done) {
    done(null, JSON.stringify(user));
});
 
 
passport.deserializeUser(function (data, done) {
    try {
        done(null, JSON.parse(data));
    } catch (e) {
        done(err)
    }
});

module.exports = function (app) {
};