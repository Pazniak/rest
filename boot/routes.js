var routes      = require('../routes/index');
var users       = require('../routes/users');
var points      = require('../routes/points');
var pictures    = require('../routes/pictures');
var setup       = require('../routes/setup');
var options     = require('../routes/options');
var auth        = require('../routes/auth');

module.exports = function (app) {
    console.log('Applying routes');

    app.use('/', routes);
    app.use('/users', users);
    app.use('/points', points);
    app.use('/pictures', pictures);
    app.use('/setup', setup);
    app.use('/options', options);
    app.use('/auth', auth);
};