var path         = require('path');
var favicon      = require('serve-favicon');
var cookieParser = require('cookie-parser');
var bodyParser   = require('body-parser');
var morgan       = require('morgan');
var jwt          = require('jsonwebtoken'); // used to create, sign, and verify tokens
var config       = require("nconf");
var express      = require('express');
var ex_session   = require('express-session');
var ex_method    = require('method-override');
var passport     = require('passport');
var path         = require('path');
var flash        = require('connect-flash');

module.exports = function (app) {
    // view engine setup
    app.set('views', path.join(__dirname, '../views'));
    app.engine('html', require('ejs').renderFile);
    app.set('view engine', 'html')

    // Temporary while we don't know how to use config.
    // var sessionOptions = config.get("session");
    var sessionOptions = {
        secret : "it:demo:secret",
        key : "sid",
        cookie : {
            path : "/",
            httpOnly : true,
            maxAge : null
        },
        resave : true,
        saveUninitialized : true
    };

    // uncomment after placing your favicon in /public
    //app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
    app.use(morgan('dev'));
    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({ extended: false }));
    app.use(cookieParser());
    app.use(ex_method());
    app.use(express.static(path.join(__dirname, '../public')));
    app.use(ex_session(sessionOptions));
    app.use(flash());

    app.use(passport.initialize());
    app.use(passport.session());
};