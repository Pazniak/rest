var express      = require('express');
var config       = require('nconf');

var app = express();

config.argv()
    .env()
    .file({
        file: './config.json'
    });

require('./boot/express')(app);
require('./boot/routes')(app);
require('./boot/passport')(app);

module.exports = app;
